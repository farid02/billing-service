package com.example.billingservice.dto;

import com.example.billingservice.entities.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceResponseDTO {

    private String id;
    private BigDecimal amount;
    private Date date ;
    private Customer customer ;

}
